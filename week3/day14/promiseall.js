
const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => {
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})



const
  read = readFile('utf-8')

async function mergedContent () {
    try{
        const result = await Promise.all([
            read('FILE/file1.txt'),
            read('FILE/file2.txt'),
            read('FILE/file3.txt'),
            read('FILE/file4.txt'),
            read('FILE/file5.txt'),
            read('FILE/file6.txt'),
            read('FILE/file7.txt'),
            read('FILE/file8.txt'),
            read('FILE/file9.txt'),
            read('FILE/file10.txt')
        ])
        await writeFile('FILE/result.txt', result.join(' '))
    } catch(e) {
        throw e
    }

    return read('FILE/result.txt')
}



mergedContent()
.then (result => {
    console.log(result);
}).catch(err =>{
    console.log('Error coba lagi');
}).finally(() => {
console.log('siap');})
