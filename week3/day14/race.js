// Import fs to read/write file
const fs = require('fs')

/* Start make promise */
const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => {
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* End make promise */


/* Make options variable for fs */
const
  read = readFile('utf-8')

/* End make options variable for fs */


Promise.race([read('FILE/file1.txt'), read('FILE/file2.txt'), read('FILE/file3.txt'), read('FILE/file4.txt'), read('FILE/file5.txt'), read('FILE/file6.txt'), read('FILE/file7.txt'), read('FILE/file8.txt'), read('FILE/file9.txt'), read('FILE/file10.txt')])
.then((value) =>{
    console.log(value);
})
.catch(ettot => {
    console.log(error);
})