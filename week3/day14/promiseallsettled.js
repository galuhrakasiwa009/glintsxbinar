const fs = require('fs')


const readFile = options => file => new Promise((resolve, reject) => {
    fs.readFile(file, options, (err, content) => {
        if (err) return reject(err)
        return resolve(content)
    })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, err => {
        if (err) return reject(err)
        return resolve()
    })
})

const
    read = readFile('utf-8'),
    files = ['FILE/file1.txt', 'FILE/file2.txt', 'FILE/file3.txt', 'FILE/file4.txt', 'FILE/file5.txt', 'FILE/file6.txt', 'FILE/file7.txt', 'FILE/file8.txt', 'FILE/file9.txt', 'FILE/file10.txt']


Promise.allSettled(files.map(file => read(`${file}`)))
    .then(results => {
        console.log(results)
    })