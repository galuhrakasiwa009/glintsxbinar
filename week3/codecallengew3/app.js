
const express = require('express') 
const app = express() 
const helloRoutes = require('./routes/rakaRoutes.js') 
const indexRoutes = require('./routes/siwaRoutes.js') 

app.use(express.static('public')); 


app.use('/', indexRoutes)


app.use('/raka', helloRoutes)

app.listen(3000) 
