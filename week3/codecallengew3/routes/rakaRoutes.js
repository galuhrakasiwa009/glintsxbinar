
const express = require('express') 
const router = express.Router() 
const HelloController = require('../controllers/rakaController.js') 

router.get('/', HelloController.hello) 

module.exports = router; 
