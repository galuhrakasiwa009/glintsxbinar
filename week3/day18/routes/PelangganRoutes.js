const express = require('express') // Import expresss
const router = express.Router() // Make a router
const PelangganController = require('../controllers/PelangganController.js') // Import PelangganController

router.get('/', PelangganController.getAll) // if acessing localhost:3000/transaksi, it will do function getAll() in PelangganController class
router.get('/:id', PelangganController.getOne) // if acessing localhost:3000/transaksi/:id, it will do function getOne() in PelangganController class
router.post('/create', PelangganController.create) // if acessing localhost:3000/transaksi/:id, it will do function create() in PelangganController class
router.put('/update', PelangganController.create) // if acessing localhost:3000/update/:id, it will do function update() in pelangganController class
module.exports = router; // Export router

