const connection = require('../models/connection.js') // import connection

class PelangganController {

    // Function getAll transaksi table
    async getAll(req, res) {
        try {
            var sql = "SELECT * FROM pelanggan ORDER by id" // make an query varible

            // Run query
            connection.query(sql, function (err, result) {
                if (err) throw err; // If error

                // If success it will return JSON of result
                res.json({
                    status: "success",
                    data: result
                })
            });
        } catch (e) {
            // If error will be send Error JSON
            res.json({
                status: "Error"
            })
        }
    }

    // Function getOne transaksi table
    async getOne(req, res) {
        try {
            var sql = "SELECT * FROM pelanggan WHERE id = ?" // make an query varible

            // Run query
            connection.query(sql, [req.params.id], function (err, result) {
                if (err) throw err; // If error

                // If success it will return JSON of result
                res.json({
                    status: "success",
                    data: result[0]
                })
            });
        } catch (e) {
            // If error will be send Error JSON
            res.json({
                status: "Error"
            })
        }
    }


    async create(req, res) {
        try {
            connection.query(
                'INSERT INTO pelanggan(nama) VALUES (?)',
                [req.body.nama],
                (error, result) => {
                    if (error) throw error;

                    res.json({
                        status: "Success",
                        data: result
                    })
                }
            )
        } catch (e) {
            res.json({
                status: "Error"
            })
        }
    }
    // Function update transaksi table
  async update(req, res) {
    try {

      var sql = 'UPDATE pelanggan nama = ? WHERE id = ?'

      connection.query(
        sql,
        [req.body.nama],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }

}

module.exports = new PelangganController;