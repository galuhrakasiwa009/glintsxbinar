const connection = require('../models/connection.js') // import connection

class PemasokController {

  // Function getAll transaksi table
  async getAll(req, res) {
    try {
      var sql = "SELECT pelanggan .id AS id, pelanggan.nama AS nama_pelanggan FROM transaksi  = p.id ORDER by t.id" // make an query varible

      // Run query
      connection.query(sql, function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  // Function getOne transaksi table
  async getOne(req, res) {
    try {
      var sql = "JOIN pelanggan ON transaksi.id_pelanggan = pelanggan.id  = p.id WHERE t.id = ?" // make an query varible

      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result[0]
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  async create(req, res) {
    try {
      connection.query(
        'INSERT INTO transaksi(id_barang, id_pelanggan, jumlah, total) VALUES (?, ?, ?, ?)',
        [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, req.body.total],
        (error, result) => {
          if (error) throw error; // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

}

module.exports = new PemasokController;
