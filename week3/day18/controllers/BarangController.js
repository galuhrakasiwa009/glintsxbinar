const connection = require('../models/connection.js') // import connection

class BarangController {

  // Function getAll transaksi table
  async getAll(req, res) {
    try {
      var sql = "SELECT barang.id as id_barang, barang.nama as nama_barang, barang.harga, pemasok.nama as nama_pemasok FROM barangJOIN pemasok ON barang.id_pemasok = pemasok.id" // make an query varible

      // Run query
      connection.query(sql, function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  // Function getOne transaksi table
  async getOne(req, res) {
    try {
      var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?" // make an query varible

      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result[0]
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  async create(req, res) {
    try {
      connection.query(
        'INSERT INTO transaksi(id_barang, id_pelanggan, jumlah, total) VALUES (?, ?, ?, ?)',
        [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, req.body.total],
        (error, result) => {
          if (error) throw error; // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

}

module.exports = new BarangController;
