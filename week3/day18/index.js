const express = require('express') // Import Express
const app = express() // Create app from express
const transaksiRoutes = require('./routes/transaksiRoutes.js') // Import transaksiRoutes
const pelangganRoutes = require('./routes/PelangganRoutes.js')

app.use(express.urlencoded({extended:false}))   // harus ada untuk create
app.use('/transaksi', transaksiRoutes) // If accessing localhost:3000/transaksi/*, it will use transaksiRoutes
app.use('/pelanggan', pelangganRoutes)

app.listen(3000) // make application have port 3000
