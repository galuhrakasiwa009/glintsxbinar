const express = require('express') // Import express
const router = express.Router() // Make express router
const BarangController = require('../controllers/pelangganController.js') // Import transaksiController from controllers directory
const barangValidator = require('../middlewares/validators/pelangganValidator.js') // Import transaksiValidator

router.get('/', BarangController.getAll) // If accessing localhost:3000/transaksi/, it will go to getAll method in TransaksiController
router.get('/:id', BarangController.getOne) // If accessing localhost:3000/transaksi/:id, it will go to getOne
router.post('/create', barangValidator.create, BarangController.create) // If accessing localhost:3000/create, it will go to transaksiValidator and create function in controller
router.put('/update/:id', barangValidator.update,BarangController.update) // If accessing localhost:3000/update/:id, it will go to transaksiValidator and update function in controller
router.delete('/delete/:id', BarangController.delete) // If accessing localhost:3000/delete/:id, it will go to delete function in transaksiController

module.exports = router; // Export router
