const express = require('express') // Import express
const router = express.Router() // Make router from app
const PemasokController = require('../controllers/pemasokController.js') // Import PemasokController
const pemasokValidator = require('../middlewares/validators/pemasokValidator.js') // Import validator to validate every request from user

router.get('/', PemasokController.getAll) // If accessing localhost:3000/pemasok, it will call getAll function in PemasokController class
router.get('/:id', PemasokController.getOne) // If accessing localhost:3000/pemasok/:id, it will call getOne function in PemasokController class
router.post('/create', pemasokValidator.create, PemasokController.create) // If accessing localhost:3000/pemasok/create, it will call create function in PemasokController class
router.put('/update/:id', pemasokValidator.update, PemasokController.update) // If accessing localhost:3000/pemasok/update/:id, it will call update function in PemasokController class
router.delete('/delete/:id', PemasokController.delete) // If accessing localhost:3000/pemasok/delete/:id, it will call delete function in PemasokController class

module.exports = router; // Export router
