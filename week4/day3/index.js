const express = require('express')
const app = express()
const transaksiRoutes = require('./routes/transaksiRoutes.js')
const barangRoutes = require('./routes/barangRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')
const pelangganRoutes = require('./routes/pelangganRoutes.js')

app.use(express.urlencoded({extended: false}));

app.use('/transaksi', transaksiRoutes)
app.use('/barang', barangRoutes)
app.use('/pemasok', pemasokRoutes)
app.use('/pelanggan', pelangganRoutes)

app.listen(3000)
