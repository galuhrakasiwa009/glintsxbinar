const connection = require('../models/connection.js')

class PemasokController {
  async getAll(req, res) {
    try {
      var sql = "SELECT * FROM pemasok p"

      connection.query(sql, function(err, result) {
        if (err) throw err;
        res.json({
          status: "Success",
          data: result
        })
      });
    } catch (e) {
      res.json({
        status: "Error"
      })
    }
  }

  async getOne(req, res) {
    try {
      var sql = "SELECT * FROM pemasok p WHERE p.id = ?"

      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err;

        res.json({
          status: "Success",
          data: result[0]
        })
      })
    } catch (e) {
      res.json({
        status: "Error"
      })
    }
  }

  async create(req, res) {
    try {
      connection.query(
        'INSERT INTO pemasok(nama) VALUES (?)',
        [req.body.nama],
        (error, result) => {
          if(error) throw error;

          res.json({
            status: "Success",
            data: result
          })
        }
      )
    } catch (e) {
      res.json({
        status: "Error"
      })
    }
  }

  async update(req, res) {
    try{
      var sql ='UPDATE pemasok p SET nama = ? WHERE id = ?'

      connection.query(
        sql, [req.body.nama, req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          }

          res.json({
            status: "Success",
            data: result
          })
        }
      )
    } catch (e) {
      res.json({
        status: "Error",
        error: err
      })
    }
  }

  async delete(req, res) {
    try {
      var sql = 'DELETE FROM pemasok p WHERE id = ?'

      connection.query(
        sql,
        [req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          }
          res.json({
            status: "Success",
            data: result
          })
        }
      )
    } catch (err) {
      res.json({
        status: "Error",
        error: err
      })
    }
  }
}

module.exports = new PemasokController
