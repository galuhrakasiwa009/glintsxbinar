const connection = require('../models/connection.js')

class pelangganController {
  async getAll(req, res) {
    try {
      var sql = "SELECT * FROM pelanggan p"

      connection.query(sql, function(err, result) {
        if (err) throw err;
        res.json({
          status: "Success",
          data: result
        })
      });
    } catch (e) {
      res.json({
        status: "Error"
      })
    }
  }

  async getOne(req, res) {
    try {
      var sql = "SELECT * FROM pelanggan p WHERE p.id = ?"

      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err;

        res.json({
          status: "Success",
          data: result[0]
        })
      })
    } catch (e) {
      res.json({
        status: "Error"
      })
    }
  }

  async create(req, res) {
    try {
      var sql = 'SELECT p.nama FROM pelanggan p WHERE p.id = ?'

      connection.query(sql, [req.body.nama_pelanggan], function(err, result) {
        if (err) {
          res.json({
            status:"Error",
            error: err
          });
        }

        var sqlInsert = 'INSERT INTO pelanggan(nama) VALUES (?)'

        connection.query(
          sqlInsert, [req.body.nama],
          (err, result) => {
            if (err) {
              res.json({
                status: "Error",
                error: err
              });
            }
            var sqlSelect = 'SELECT p.id, p.nama FROM pelanggan p WHERE p.id = ?'

            connection.query(sqlSelect, [result.insertId], function(err, result) {
              if (err) {
                res.json({
                  status: "Error",
                  error: err
                });
              }
              res.json({
                status: "Success add data",
                data: result
              })
            });
          }
        )
      });
    } catch (e) {
      res.json({
        status: "Error"
      })
    }
  }

  async update(req, res) {
    try {
      var sqlUpdate = 'UPDATE pelanggan p SET nama = ? WHERE id = ?'

      connection.query(
        sqlUpdate,
        [req.body.nama, req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          }

          var sqlSelect = 'SELECT p.id, p.nama FROM pelanggan p WHERE p.id = ?'

          connection.query(sqlSelect, [req.params.id], function(err, result) {
            if (err) {
              res.json({
                status: "Error",
                error: err
              });
            }

            res.json({
              status: "Succes add data",
              data: result[0]
            })
          });
        }
      )
    } catch (e) {
      res.json({
        status: "Error",
        error: err
      })
    }
  }

  async delete(req, res) {
    try {
      var sql = 'DELETE FROM pelanggan p WHERE id = ?'

      connection.query(
        sql,
        [req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          }
          res.json({
            status: "Success",
            data: result
          })
        }
      )
    } catch (err) {
      res.json({
        status: "Error",
        error: err
      })
    }
  }
}

module.exports = new pelangganController
