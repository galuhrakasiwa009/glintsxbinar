const express = require('express') // Import express
const router = express.Router() // Make router from app
const barangController = require('../controllers/barangController.js') // Import barangController
const barangValidator = require('../middlewares/validators/barangValidator.js') // Import validator to validate every request from user

router.get('/', barangController.getAll) // If accessing localhost:3000/barang, it will call getAll function in barangController class
router.get('/:id', barangController.getOne) // If accessing localhost:3000/barang/:id, it will call getOne function in barangController class
router.post('/create', barangValidator.create, barangController.create) // If accessing localhost:3000/barang/create, it will call create function in barangController class
router.put('/update/:id', barangValidator.update, barangController.update) // If accessing localhost:3000/barang/update/:id, it will call update function in barangController class
router.delete('/delete/:id', barangController.delete) // If accessing localhost:3000/barang/delete/:id, it will call delete function in barangController class

module.exports = router; // Export router