const express = require('express') // Import express
const router = express.Router() // Make router from app
const pelangganController = require('../controllers/pelangganController.js') // Import pelangganController
const pelangganValidator = require('../middlewares/validators/pelangganValidator.js') // Import validator to validate every request from user

router.get('/', pelangganController.getAll) // If accessing localhost:3000/pelanggan, it will call getAll function in pelangganController class
router.get('/:id', pelangganController.getOne) // If accessing localhost:3000/pelanggan/:id, it will call getOne function in pelangganController class
router.post('/create', pelangganValidator.create, pelangganController.create) // If accessing localhost:3000/pelanggan/create, it will call create function in pelangganController class
router.put('/update/:id', pelangganValidator.update, pelangganController.update) // If accessing localhost:3000/pelanggan/update/:id, it will call update function in pelangganController class
router.delete('/delete/:id', pelangganController.delete) // If accessing localhost:3000/pelanggan/delete/:id, it will call delete function in pelangganController class

module.exports = router; // Export router