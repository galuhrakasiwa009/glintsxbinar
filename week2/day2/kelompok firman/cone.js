// const index = require('./index.js')
// const readline = require('readline');
// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout
// });

const index=require('./index.js');

function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}


const pi = 3.14;
function kerucut(pi, radius, height) {
    return 1/3 * pi * radius * radius * height
}

  
function inputradius() {
        index.rl.question(`Radius:`, radius => {
            if (!isNaN(radius) && !isEmptyOrSpaces(radius)) {
                inputheight(radius)
            } else {
                console.log("Radius must be a number\n");
                inputradius()
            }
        })
}

function inputheight(radius) {
    index.rl.question(`Height:`, height => {
        if (!isNaN(height) && !isEmptyOrSpaces(height)) {
            console.log(`Volume of the cone is:${kerucut(pi,radius, height)}`);
            index.rl.close()
        } else {
            console.log("Height must be a number\n");
            inputheight(radius)
          }
    })
  }
  
//   inputradius()
  module.exports.rumuscone = inputradius
