// const readline=require("readline");
// const rl=readline.createInterface({
//     input:process.stdin,
//     output:process.stdout
// });
const index=require('./index.js')

function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
} 
function vball(r) {
    return 3.14*r**3*4/3
    
}

function inputradius() {
    index.rl.question("radius : ", r=>{
        if (!isNaN(r) && !isEmptyOrSpaces(r)) {
            console.log(`Volume= ${vball(r)}`);
            index.rl.close();
        } else {
            console.log("radius must be a number");
            inputradius(r)
        }
    })
}
// inputradius();
module.exports.vball=inputradius