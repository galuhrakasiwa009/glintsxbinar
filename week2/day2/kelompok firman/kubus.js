// const readline = require('readline');
// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout
// })

const index=require('./index.js')

function isEmptyOrSpaces(str){
  return str == null || str.match(/^ *$/) !== null;
}

function volumeKubus(s) {
  return s ** 3
}

function inputSisi(s){
  index.rl.question("Masukkan sisi: ", s => {
    if (!isNaN(s) && !isEmptyOrSpaces(s)) {
      console.log("Volume Kubus adalah ", + volumeKubus(s));
      index.rl.close()
    } else {
      console.log("Sisi harus angka!");
      inputSisi(s)
    }
  })
}

// console.log("Volume Kubus");
// console.log("=============");
// inputSisi();

module.exports.rumuskubus = inputSisi
