const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!


 1 step membuat log untuk data lama
 * */
console.log(`Old Data`);
console.log(data.dataSort);
// console.log a


//2 step filter nilai null

// Optional
function clean() {
  return data.dataSort. filter(i => i != null);
}
//3 step buat function baru untuk data baru

function databaru() {
  var dataBaru = []
  dataBaru = clean(data)
  console.log(dataBaru);
}

// step 4 jalankan fungsi sortAscending

// Should return array
function sortAscending() {
  var b = data.dataSort.filter(i => i != null);
  for(var i = 0; i < b.length; i++) {
      for(var j=i+1; j < b.length; j++) {
          if(b[i] > b[j]) {
              var grs = b[i];
              b[i] = b[j];
              b[j] = grs;
          }
      }
  }
  console.log(b);
  //consol.log b
  //grs hanya variabel bisa di ganti apa aja
}

//step 5 jalankan fungsi sortDiscending

// Should return array
function sortDecending() {
  var c = data.dataSort.filter(i => i != null);
  for(var i = 0; i < c.length; i++) {
      for(var j=i+1; j < c.length; j++) {
          if(c[i] < c[j]) {
              var grs = c[i];
              c[i] = c[j];
              c[j] = grs;
          }
      }
  }
  console.log(c);
  //consol.log c

  // Code Here


}

// output
console.log("Old Data filter Null");
databaru()

console.log("Data Ascending");
sortAscending()

console.log("data Discending");
sortDecending()

// DON'T CHANGE
test(sortAscending, sortDecending, data);